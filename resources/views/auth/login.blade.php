<!DOCTYPE html>

<html lang="en">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>Login</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>

		<link href="{{ asset('theme_assets/vendors/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />

		<link rel="shortcut icon" href="../../../assets/demo/media/img/logo/favicon.ico" />
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" id="m_login" 
            style="background-image: url({{ asset('theme_assets/app/media/img//bg/bg-1.jpg') }});">
				<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
					<div class="m-login__container">
						<div class="m-login__logo">
							<a href="#">
								
							</a>
						</div>
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">Sign In</h3>
              </div>
              
              
							<form class="m-login__form m-form" method="POST" action="{{ route('login') }}">
                 {{ csrf_field() }}
                 @if(session()->has('login_error'))
                  <div class="alert alert-success">
                    {{ session()->get('login_error') }}
                  </div>
                @endif
                <div class="form-group{{ $errors->has('identity') ? ' has-error' : '' }} m-form__group">
									<input  value="{{ old('identity') }}"  class="form-control m-input" type="identity"  placeholder="Email or Username" name="identity" autofocus autocomplete="off">
                </div>
                 @if ($errors->has('identity'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('identity') }}</strong>
                                    </span>
                  @endif

								<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
                </div>
                
                  @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                  @endif
						
								<div class="m-login__form-action">
									<button type="submit"  id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary">Sign In</button>
								</div>
							</form>
						</div>
					
					</div>
				</div>
			</div>
		</div>

	</body>
</html>
