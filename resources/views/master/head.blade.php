<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
<script>
    WebFont.load({
    google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
    active: function() {
        sessionStorage.fonts = true;
    }
});
</script>
<link href="{{ asset('theme_assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('theme_assets/demo/demo7/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('theme_assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="assets/demo/demo7/media/img/logo/favicon.ico" />