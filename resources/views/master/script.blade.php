<script src="{{ asset('theme_assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{ asset('theme_assets/demo/demo7/base/scripts.bundle.js')}}" type="text/javascript"></script>
<script src="{{ asset('theme_assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
<script src="{{ asset('theme_assets/app/js/dashboard.js')}}" type="text/javascript"></script>