<button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-light ">

	<!-- BEGIN: Brand -->
	<div class="m-brand  m-brand--skin-light ">
		<a href="index.html" class="m-brand__logo">
			DODO
		</a>
	</div>
	<!-- END: Brand -->

	<!-- BEGIN: Aside Menu -->
	<div id="m_ver_menu" class="m-aside-menu m-aside-menu--skin-light m-aside-menu--submenu-skin-light " data-menu-vertical="true" m-menu-scrollable="true" m-menu-dropdown-timeout="500">
		<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
		
			<li class="m-menu__item  m-menu__item--submenu" m-menu-submenu-toggle="click" m-menu-link-redirect="1">
				<a href="javascript:;" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-add"></i>
				</a>
				<div class="m-menu__submenu ">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" m-menu-link-redirect="1">
							<span class="m-menu__link">
								<span class="m-menu__link-text">CRUD TEMPLATE</span>
							</span>
						</li>
						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1">
							<a href="{{ route('crud.index') }}" class="m-menu__link ">
								<i class="m-menu__link-icon la la-commenting"></i>
								<span class="m-menu__link-text">List Data</span>
							</a>
						</li>
					</ul>
				</div>
			</li>
			
		</ul>
	</div>

	<!-- END: Aside Menu -->
</div>
<div class="m-aside-menu-overlay"></div>